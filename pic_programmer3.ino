//////////////////////////////////////////////////////////////////////
// TODO (chs): sort out factory reset
// TODO (chs): sort out different EEPROM modes
// TODO (chs): make read output a hex file
// DONE (chs): make a common include file for the command definitions shared with the PC code
//////////////////////////////////////////////////////////////////////

// Arduino -> ICSP connections

#define VPP     8
#define VDD     9
#define DATA    10
#define CLOCK   11
#define TRIGGER 12
#define ACTIVITY 13

// shared enum of command list

#include "../picpgm/picpgm/picpgm/cmds.cs"

// PIC ICSP commands

#define LOAD_CONFIG                     B000000
#define LOAD_DATA_PROGRAM               B000010
#define LOAD_DATA_DATA                  B000011
#define INCREMENT_ADDRESS               B000110
#define READ_DATA_PROGRAM               B000100
#define READ_DATA_DATA                  B000101

//#define BEGIN_ERASE_PROGRAMMING         B001000
//#define BEGIN_PROGRAMMING_ONLY          B011000

#define BEGIN_ERASE_PROGRAMMING         B001000 // TODO (chs): these need to be different depending on the PIC
#define BEGIN_PROGRAMMING_ONLY          B001000

#define BULK_ERASE_PROGRAM              B001001
#define BULK_ERASE_DATA                 B001011
#define BULK_ERASE_SETUP1               B000001
#define BULK_ERASE_SETUP2               B000111

// Delays (from the datasheet, mostly, some fudging)

#define delay_powerUp()                 delay(160)
#define delay_powerDown()               delay(200)
#define delay_Tppdp()                   delayMicroseconds(10)
#define delay_Thld0()                   delayMicroseconds(10)
#define delay_Tset1()                   delayMicroseconds(3)
#define delay_Thld1()                   delayMicroseconds(3)
#define delay_Tdly2()                   delayMicroseconds(3)
#define delay_Tera()                    delay(5)
#define delay_Tprog()                   delay(8)
#define delay_TeraTprog()               delay(15)   // datasheet says 13 but 15 seems more reliable
#define delay_TeraTprogBulk()           delay(20)   // datasheet says 13 but needs to be 20

//////////////////////////////////////////////////////////////////////
// one time init

int activity = 0;

void setup() {

    // all pins to output

    pinMode(VPP, OUTPUT);
    pinMode(VDD, OUTPUT);
    pinMode(DATA, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(TRIGGER, OUTPUT);
    pinMode(ACTIVITY, OUTPUT);

    digitalWrite(ACTIVITY, LOW);

    // setup serial port

    Serial.begin(57600);

    while(!Serial) {
    }

    // power off the pic

    SwitchOffPIC();
    //EnterProgrammingMode();
    //while(1);

}

//////////////////////////////////////////////////////////////////////
// Control VPP & VDD

void VPP_On()
{
    digitalWrite(TRIGGER, HIGH);
    digitalWrite(ACTIVITY, HIGH);
    digitalWrite(VPP, LOW);
}


void VPP_Off()
{
    digitalWrite(TRIGGER, LOW);
    digitalWrite(ACTIVITY, LOW);
    digitalWrite(VPP, HIGH);
}

void VDD_On()
{
    digitalWrite(VDD, LOW);
}

void VDD_Off()
{
    digitalWrite(VDD, HIGH);
}

//////////////////////////////////////////////////////////////////////
// switch off the PIC

void SwitchOffPIC()
{
    VPP_Off();
    VDD_Off();
    pinMode(DATA, INPUT);
    pinMode(CLOCK, INPUT);
    //digitalWrite(CLOCK, LOW);
    //digitalWrite(DATA, LOW);
}

//////////////////////////////////////////////////////////////////////
// get pic ready for programming

void EnterProgrammingMode() {

    // all off except VPP
    SwitchOffPIC();

    delay_powerDown();

    pinMode(DATA, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    digitalWrite(CLOCK, LOW);
    digitalWrite(DATA, LOW);

    // power on, VPP high simultaneously, seems to work
    VDD_On();
    VPP_On();

    delay_powerUp();
}

//////////////////////////////////////////////////////////////////////
// run the code on the pic

void RunPIC() {

    // everything off
    SwitchOffPIC();

    delay_powerDown();

    // switch on the pic, not VPP though
    VDD_On();
}

//////////////////////////////////////////////////////////////////////
// Read a byte from the serial port

byte ReadSerialByte() {
    //digitalWrite(ACTIVITY, ((++activity) >> 2) & 1);
    do ; while(Serial.available() == 0);
    return Serial.read();
}

//////////////////////////////////////////////////////////////////////
// Send a byte

void SendByte(byte b) {
    Serial.write(b);
}

//////////////////////////////////////////////////////////////////////
// Send a 16bit word to the PC on the serial port
// LSB then MSB from PC <-> Arduino

void SendWord(uint16_t v) {
    Serial.write(v & 0xff);
    Serial.write(v >> 8);
}

//////////////////////////////////////////////////////////////////////
// Get a 16bit word from the PC on the serial port

uint16_t ReceiveWord() {
    uint16_t lsb = (uint16_t)ReadSerialByte();
    uint16_t msb = (uint16_t)ReadSerialByte();
    return (msb << 8) | lsb;
}

//////////////////////////////////////////////////////////////////////
// command responses

//////////////////////////////////////////////////////////////////////
// ACK, 0 - no data back, just ack

void Reply0(byte command) {
    Serial.write(command ^ 0xff);
    SendWord(0);
}

//////////////////////////////////////////////////////////////////////
// ACK, 2, UINT16 - 16bit word reply

void Reply2(byte command, uint16_t data) {
    Serial.write(command ^ 0xff);
    SendWord(2);
    SendWord(data);
}

//////////////////////////////////////////////////////////////////////
// ACK, N, N bytes - N words of data in the reply

void ReplyN(byte command, uint16_t len) {
    Serial.write(command ^ 0xff);
    SendWord(len);
}

//////////////////////////////////////////////////////////////////////
// main loop

void loop() {
    uint16_t got, config;

    if(Serial.available() == 0) {
        return;
    }

    byte command = Serial.read();

    switch (command) {

        case ping:
            Reply0(command);
            break;

        case off:
            SwitchOffPIC();
            Reply0(command);
            break;

        case wake:
            EnterProgrammingMode();
            Reply0(command);
            break;

        case erase:
            FullEraseProgramMemory();   // TODO (chs) work out why it needs erasing twice
            EnterProgrammingMode();
            FullEraseProgramMemory();
            EnterProgrammingMode();
            Reply0(command);
            break;

        case readProgram:
            Reply2(command, ReadDataFromProgramMemory());
            break;

        case loadCodeBlock:
            got = ReceiveWord();
            for(int i=0; i<got/2; ++i) {
                uint16_t wrd = ReceiveWord();
                LoadDataCommandForProgramMemory(wrd);
                BeginProgrammingOnlyCycle();
                IncrementAddress();
            }
            Reply2(command, got);
            break;

        case loadCodeWord:
            got = ReceiveWord();
            LoadDataCommandForProgramMemory(got);
            Reply2(command, got);
            break;

        case loadDataWord:
            got = ReceiveWord();
            LoadDataCommandForDataMemory(got);
            Reply2(command, got);
            break;

        case programWord:
            BeginProgrammingOnlyCycle();
            Reply0(command);
            break;

        case eraseProgramWord:
            BeginEraseProgrammingCycle();
            Reply0(command);
            break;

        case readCode:
            got = ReceiveWord();    // how many to read
            LoadDataCommandForProgramMemory(0x3fff);
            ReplyN(command, got);
            for(int i=0; i<got / 2; ++i) {
                SendWord(ReadDataFromProgramMemory());
                IncrementAddress();
            }
            break;

        case readData:
            got = ReceiveWord();
            LoadDataCommandForDataMemory(0);
            ReplyN(command, got);
            for(int i=0; i<got; ++i) {
                SendByte(ReadDataFromDataMemory());
                IncrementAddress();
            }
            EnterProgrammingMode();
            break;

        case readConfig:
            LoadConfiguration(0);
            AddPC(6);
            ReplyN(command, 4);
            SendWord(ReadDataFromProgramMemory());
            IncrementAddress();
            SendWord(ReadDataFromProgramMemory());
            break;

        // remove code protection
        case resetConfig:
            FactoryReset();
            Reply0(command);
            break;

        case incrementAddress:
            IncrementAddress();
            Reply0(command);
            break;

        // 'c' write config value
        case writeConfig:
            got = ReceiveWord();
            LoadConfiguration(ReceiveWord());
            AddPC(7);
            BeginEraseProgrammingCycle();
            EnterProgrammingMode();
            Reply0(command);
            break;

        // 'R' - reset by powering the chip off and on again
        case execute:
            RunPIC();
            Reply0(command);
            break;

        default:
            Reply0(command ^ 0xff);
            break;
    }
}

//////////////////////////////////////////////////////////////////////

void WriteBit(uint16_t bit) {
    delay_Thld0();
    digitalWrite(CLOCK, HIGH);
    digitalWrite(DATA, bit);
    delay_Tset1();
    digitalWrite(CLOCK, LOW);
    delay_Thld1();
    digitalWrite(DATA, bit);
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadBit() {
    delay_Thld0();
    digitalWrite(CLOCK, HIGH);
    delay_Tset1();
    byte v = digitalRead(DATA);
    digitalWrite(CLOCK, LOW);
    delay_Thld1();
    return (uint16_t)v;
}

//////////////////////////////////////////////////////////////////////
// bits from LSB to MSB from Arduino <-> PIC

void WriteBits(uint16_t data, int n) {
    for(; n > 0; --n) {
        WriteBit(data & 1);
        data >>= 1;
    }
}

//////////////////////////////////////////////////////////////////////
// send 14 bits to PIC

void WriteWord14(uint16_t v) {
    v = (v << 1) & 0x7ffe;
    WriteBits(v, 16);
}

//////////////////////////////////////////////////////////////////////
// PC += n

void AddPC(int n)
{
    for(int i=0; i<n; ++i) {
        IncrementAddress();
    }
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadBits(int n) {
    uint16_t v = 0;
    for(int i = 0; i < n; ++i) {
        v |= ReadBit() << i;
    }
    return v;
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadWord() {
    pinMode(DATA, INPUT);
    uint16_t v = ReadBits(16);
    pinMode(DATA, OUTPUT);
    return (v >> 1) & 0x3fff;
}

//////////////////////////////////////////////////////////////////////

void WriteCmd(uint8_t cmd) {
    digitalWrite(DATA, LOW);    // ? spurious - wake & writebit do it anyway
    for(int i=0; i<6; ++i) {
        WriteBit(cmd & 1);
        cmd >>=1 ;
    }
    delay_Tdly2();
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadDataFromProgramMemory() {
    WriteCmd(READ_DATA_PROGRAM);
    return ReadWord();
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadDataFromDataMemory() {
    WriteCmd(READ_DATA_DATA);
    return ReadWord();
}

//////////////////////////////////////////////////////////////////////

void LoadDataCommandForProgramMemory(uint16_t v) {
    WriteCmd(LOAD_DATA_PROGRAM);
    WriteWord14(v);
}

//////////////////////////////////////////////////////////////////////

void LoadDataCommandForDataMemory(uint16_t v) {
    WriteCmd(LOAD_DATA_DATA);
    WriteWord14(v);
}

//////////////////////////////////////////////////////////////////////

void BeginProgrammingOnlyCycle() {
    WriteCmd(BEGIN_PROGRAMMING_ONLY);
    delay_Tprog();
}

//////////////////////////////////////////////////////////////////////

void BeginEraseProgrammingCycle() {
    WriteCmd(BEGIN_ERASE_PROGRAMMING);
    delay_TeraTprog();
}

//////////////////////////////////////////////////////////////////////

void BulkEraseProgramMemory() {
    WriteCmd(BULK_ERASE_PROGRAM);
    delay_TeraTprog();
}

//////////////////////////////////////////////////////////////////////

void FullEraseProgramMemory() {
    uint16_t Erase = 0x3fff;
    LoadDataCommandForProgramMemory(Erase);
    BulkEraseProgramMemory();
    BeginProgrammingOnlyCycle();    // TODO (chs): remove this? seems redundant considering the PIC is reset just after
}

//////////////////////////////////////////////////////////////////////

void IncrementAddress() {
    WriteCmd(INCREMENT_ADDRESS);
}

//////////////////////////////////////////////////////////////////////

void LoadConfiguration(uint16_t v) {
    WriteCmd(LOAD_CONFIG);
    WriteWord14(v);
}

//////////////////////////////////////////////////////////////////////

void FactoryReset() {
    LoadConfiguration(0xffff);
    for(int i=0; i<7; ++i) {
        IncrementAddress();
    }
    WriteCmd(BULK_ERASE_SETUP1);
    WriteCmd(BULK_ERASE_SETUP2);
    BeginEraseProgrammingCycle();
    delay_TeraTprogBulk();
    WriteCmd(BULK_ERASE_SETUP1);
    WriteCmd(BULK_ERASE_SETUP2);
    delay_TeraTprogBulk();
    EnterProgrammingMode();
}